@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        <div class="card-header">{{ __('Students List') }}</div>

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Thumb</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Price</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php $total = 0 ?>
                            @if(session('cart'))
                                @foreach(session('cart') as $id => $details)
                                    <?php $total += $details['quantity'] * $product->price ?>

                                <tr>
                                    <th>{{ $product->id }}</th>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->thumb }}</td>
                                    <td data-th="Quantity">
                                        <input type="number" value="{{ $details['quantity'] }}" class="form-control quantity" />
                                    </td>
                                    <td>{{ $product->price }}</td>
                                </tr>
                                @endforeach
                            @endif
                            </tbody>

                            <tfoot>
                            <tr class="visible-xs">
                                <td class="text-center"><strong>Total {{ $total }}</strong></td>
                            </tr>
                            <tr>
                                <td><a href="{{ url('products') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                                <td colspan="2" class="hidden-xs"></td>
                                <td class="hidden-xs text-center"><strong>Total ${{ $total }}</strong></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
