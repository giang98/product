@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        <div class="card-header">{{ __('Students List') }}</div>

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">Index</th>
                                <th scope="col">Name</th>
                                <th scope="col">Thumb</th>
                                <th scope="col">Price</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach ($products as $key=>$product)
                                <tr>
                                    <th>{{ $key+1 }}</th>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->thumb }}</td>
                                    <td> {{ $product->price }}</td>
                                    <td><a href="{{ url('add-to-cart/'.$product->id) }}"><i class="fas fa-cart-plus"></i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
