<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Products::all();
        return view('product', compact('products'));
    }

    public function cart()
    {
        return view('cart');
    }

    public function addToCart($id)
    {
        $product = Products::find($id);

        if(!$product) {

            abort(404);

        }

        $cart = session()->get('cart', []);

//        $cart[$id] = [
//            'quantity' =>  ($cart[$id]['quantity'] ?? 0) + 1
//        ];

        if(isset($cart[$id])) {
            $cart[$id]['quantity']++;
        } else {
            $cart[$id] = ['quantity' => 1];
        }

        session()->put('cart', $cart);

        return view('cart', compact('cart', 'product'));
    }
}
